# [![](https://codeberg.org/jaahas/tekstiksi/raw/branch/main/public/favicon.svg)](https://jaahas.codeberg.page/tekstiksi/) Tekstiksi

Muuttaa puheen nopeasti tekstiksi.

1. Valitse äänitiedosto
2. Paina "Tekstiksi"

Helppoa.

[Demo](https://jaahas.codeberg.page/tekstiksi/)

Voit myös muotoilla sen tekstin "Muotoile"-napista järkevämpään muotoon.
(Faktuaalisuus, järkevyys ja häiritsemättömyys ei taattu)

Tämä sovellus käyttää <a href="https://huggingface.co/spaces/hf-audio/whisper-large-v3">Whisperiä</a> tekstin tuottamiseen ja <a href="https://huggingface.co/spaces/Qwen/Qwen1.5-110B-Chat-demo">Qweniä</a> sen muotoiluun ilmaisien API:en kautta.